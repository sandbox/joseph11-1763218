<?php
/**
 * @file
 * site_availability.features.inc
 */

/**
 * Implements hook_node_info().
 */
function site_availability_node_info() {
  $items = array(
    'availability_log' => array(
      'name' => t('Availability log'),
      'base' => 'node_content',
      'description' => t('Logging availability'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'availability_web' => array(
      'name' => t('Availability web'),
      'base' => 'node_content',
      'description' => t('Website availability'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
