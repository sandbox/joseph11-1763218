<?php
/**
 * @file
 * site_availability.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function site_availability_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hosting|node|availability_web|default';
  $field_group->group_name = 'group_hosting';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'availability_web';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hosting',
    'weight' => '3',
    'children' => array(
      0 => 'field_ip_address',
      1 => 'field_paid_to',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_hosting|node|availability_web|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hosting|node|availability_web|form';
  $field_group->group_name = 'group_hosting';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'availability_web';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hosting',
    'weight' => '6',
    'children' => array(
      0 => 'field_ip_address',
      1 => 'field_paid_to',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_hosting|node|availability_web|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_measurement|node|availability_web|default';
  $field_group->group_name = 'group_measurement';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'availability_web';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Measurement',
    'weight' => '1',
    'children' => array(
      0 => 'field_url',
      1 => 'field_interval',
      2 => 'field_email',
      3 => 'field_timeout',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_measurement|node|availability_web|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_measurement|node|availability_web|form';
  $field_group->group_name = 'group_measurement';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'availability_web';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Measurement',
    'weight' => '1',
    'children' => array(
      0 => 'field_url',
      1 => 'field_interval',
      2 => 'field_email',
      3 => 'field_timeout',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_measurement|node|availability_web|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_system|node|availability_web|default';
  $field_group->group_name = 'group_system';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'availability_web';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'System',
    'weight' => '2',
    'children' => array(
      0 => 'field_cms',
      1 => 'field_database',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_system|node|availability_web|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_system|node|availability_web|form';
  $field_group->group_name = 'group_system';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'availability_web';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'System',
    'weight' => '2',
    'children' => array(
      0 => 'field_database',
      1 => 'field_cms',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_system|node|availability_web|form'] = $field_group;

  return $export;
}
